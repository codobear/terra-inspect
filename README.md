# Terra Inspect

Coding Bear blockchain explorer for [Terra Blockchain](https://terra.money)

This should provide (limited set of) functionality similar to [Terra Finder](https://finder.terra.money) 
([repo](https://github.com/terra-money/finder)). 

## Libraries and tools used

- [Grommet](https://v2.grommet.io/components) for UI
- [React Vis](https://uber.github.io/react-vis/documentation/getting-started/creating-a-new-react-vis-project) for Charts
- [Terra.js](https://docs.terra.money/docs/develop/sdks/terra-js/README.html) for communication with Terra Blockchain node
- [Log Finder](https://github.com/terra-money/log-finder) and [Log Finder rules](https://github.com/terra-money/log-finder-ruleset) for parsing Terra Txs
- [Jotai](https://jotai.org/) for global state management
- [React](https://reactjs.org/) scaffolded with [Create React App](https://create-react-app.dev/) ([original README.md](README-cra.md))
- and [Typescript](https://www.typescriptlang.org/docs/handbook/) of course


## NPM scripts

To run dev server run:

```
npm start
```

To build production code run:

```
npm run build
```

## TODO

- token balance of account [Mantlemint](https://github.com/terra-money/mantlemint)
- [plotting account balance in time](https://storybook.grommet.io/?path=/story/visualizations-datachart-everything--everything)
  (this would required shared state via atoms)
