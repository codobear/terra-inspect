#!/bin/bash
USER=ubuntu
HOST=terrainspect.lab.bearz.rocks
URL=${USER}@${HOST}
DEST_DIR=/home/ubuntu/terra-inspect
cd $(dirname $0)
scp dist.zip ${URL}:${DEST_DIR} && \
ssh ${URL} "cd ${DEST_DIR}/nginx-data/html && rm -rf * && unzip ../../dist.zip"
