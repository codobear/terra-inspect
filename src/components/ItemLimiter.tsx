import { BoxExtendedProps, Button } from 'grommet';
import { useState } from 'react';
import SaneBox from './SaneBox';

export type ItemLimiterProps = { items?: JSX.Element[], limit: number } & Omit<BoxExtendedProps, 'children'>;
export const ItemLimiter = ({
    items,
    limit,
    ...boxProps
}: ItemLimiterProps) => {
    const [page, setPage] = useState(1);
    if (items === undefined) {
        return null;
    }

    const hasNextPage = items.length > page * limit;
    const shownItems = !hasNextPage ? items : items.slice(0, limit * page);

    return (
        <SaneBox {...boxProps}>
            {shownItems}
            {page > 1 && (
                <Button size="small" label="Show Less" onClick={() => setPage(1)}/>
            )}
            {hasNextPage && (
                <Button size="small" label="Show More" onClick={() => setPage(page + 1)}/>
            )}
        </SaneBox>
    );
};
