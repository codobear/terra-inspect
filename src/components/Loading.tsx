import { Spinner } from 'grommet';

const Loading = () => (<Spinner size="medium"/>);

export default Loading;
