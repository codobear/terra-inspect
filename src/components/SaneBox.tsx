import { Box, BoxExtendedProps } from 'grommet';

const SaneBox = (props: BoxExtendedProps) => (<Box width={{ min: 'auto '}} height={{ min: 'auto' }} {...props} />);
export default SaneBox;
