import SaneBox from './SaneBox';
import { BoxExtendedProps, BoxProps } from 'grommet';

export type PillProps = {
    color: BoxProps['background']
} & Omit<BoxExtendedProps, 'background'>;
export const Pill = ({ color, ...props }: PillProps) => (
    <SaneBox
        background={color}
        round="xsmall"
        pad={{
            vertical: 'xxsmall',
            horizontal: 'xsmall',
        }}
        {...props}
    />
);
