import { Box } from "grommet";

export default function AppHeader(props: any) {
  return (
    <Box
      tag="header"
      direction="row"
      align="center"
      justify="between"
      background="purple!"
      pad={{ left: "medium", right: "small", vertical: "small" }}
      elevation="medium"
      height={{ min: 'auto' }}
      style={{ zIndex: "1" }}
      {...props}
    />
  );
}
