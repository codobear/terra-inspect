import {
    accountBalancePlotAtom,
    accountBalancePlotCoinAtom,
    accountBalanceTypesAtom, PlotSeries,
} from '../atoms/accountBalancePlot';
import SaneBox from './SaneBox';
import { DataChart, Select, ThemeContext } from 'grommet';
import { useAtom, useAtomValue } from 'jotai';
import { useContext, useEffect, useState } from 'react';
import {
    AreaSeries, AreaSeriesPoint, Crosshair,
    FlexibleWidthXYPlot,
    GradientDefs,
    HorizontalGridLines,
    MarkSeries,
    VerticalGridLines, XAxis,
    YAxis
} from 'react-vis';
import { normalizeColor } from 'grommet/utils';
import "react-vis/dist/style.css";

export const LineSeriesPlot = ({ data }: { data: PlotSeries[]}) => {
    const theme = useContext(ThemeContext)
    const [highlightedData, setHighlightedData] = useState<AreaSeriesPoint | undefined>(undefined);

    const c1 = normalizeColor('orange!', theme);
    const c2 = normalizeColor('red!', theme);
    const c3 = normalizeColor('yellow!', theme);
    const textColor = normalizeColor('text', theme);
    const mutedColor = normalizeColor('text', theme);
    const styleArea = { text: {fill: textColor}};
    const styleGrid = { stroke: mutedColor, color: mutedColor, opacity: 0.3 };
    const chartData = data.map(({ date, v }) => ({ x: new Date(date).getTime(), y: v }));

    // const { x: xDomain, y: yDomain } = chartData.reduce(
    //     (prev, curr): {x: [number, number], y: [number,number]} => ({
    //         x: prev === undefined ? [curr.x, curr.x] : [Math.min(curr.x, prev.x[0]), Math.max(curr.x, prev.x[1])],
    //         y: prev === undefined ? [curr.y, curr.y] : [Math.min(curr.y, prev.y[0]), Math.max(curr.y, prev.y[1])]
    //     }),
    //     undefined as (undefined | {x: [number, number], y: [number, number] })
    // ) ?? {x: [0,0], y:[0,0] };
    return (
        <SaneBox fill="horizontal" direction="row" align="center" justify="stretch">
            <FlexibleWidthXYPlot
                height={300}
                xType="time"
                margin={{ left: 100, right: 10, top: 10, bottom: 40}}
                // xDomain={xDomain}
                // yDomain={yDomain}
                onMouseLeave={() => setHighlightedData(undefined)}
            >
                <GradientDefs>
                    <linearGradient id="bg-1" x1="0" x2="0" y1="0" y2="1">
                        <stop offset="0%" stopColor={c1} stopOpacity={1} />
                        <stop offset="100%" stopColor={c2} stopOpacity={0.2} />
                    </linearGradient>
                </GradientDefs>
                <VerticalGridLines style={styleGrid} />
                <HorizontalGridLines style={styleGrid} />
                <XAxis style={styleArea} tickTotal={10} />
                <YAxis style={styleArea} />
                <AreaSeries
                    data={chartData}
                    fill="url(#bg-1)"
                    stroke={c3}
                    curve="curveStepAfter"
                    onNearestXY={(d) => setHighlightedData((d))}
                />
                <MarkSeries
                    data={chartData}
                    stroke={c3}
                    fill={c2}
                    _sizeValue={2}
                />
                {highlightedData && (
                    <Crosshair
                        values={[highlightedData]}
                        titleFormat={(ds) =>
                            ({
                                title: 'Date',
                                value: new Date(ds[0].x).toLocaleString()
                            })}
                        itemsFormat={(ds) => ds.map((d: AreaSeriesPoint) => ({
                            title: 'Balance',
                            value: d.y.toString()
                        }))}
                    />
                )}
            </FlexibleWidthXYPlot>
        </SaneBox>
    );
};


export const BalanceChart: React.FC<{}> = () => {
    const { data, max } = useAtomValue(accountBalancePlotAtom);
    const coinTypes = useAtomValue(accountBalanceTypesAtom);
    const [coinType, setCoinType] = useAtom(accountBalancePlotCoinAtom);

    useEffect(() => {
        if (coinType === null && coinTypes.length > 0) {
            setCoinType(coinTypes[0]);
        }
    }, [coinTypes, coinType, setCoinType]);

    const useGrommetPlot = false;

    return (
        <SaneBox fill="horizontal" direction="column" align="stretch" gap="xsmall" pad={{ vertical: 'large', horizontal: 'xsmall' }} border round="xsmall" elevation="medium">
            <SaneBox direction="row" justify="end">
                {coinType && (
                    <Select options={coinTypes} value={coinType} onChange={({ option }) => setCoinType(option)} />
                )}
            </SaneBox>
            {useGrommetPlot ? (
                <DataChart
                    data={data}
                    series={[
                        {
                            property: 'date',
                            label: 'Time'
                        },
                        {
                            property: 'v',
                            label: 'Balance'
                        }
                    ]}
                    // bounds="align"
                    // bounds={{y: [0, max || 0]}}
                    size={{ width: 'fill', height: 'medium' }}
                    detail
                />
            ) : (
                <LineSeriesPlot data={data} />
            )}
        </SaneBox>
    );
};
