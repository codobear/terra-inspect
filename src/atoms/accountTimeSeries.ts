import { atom } from 'jotai';
import { accountBalanceAtom, accountTxsAtom, TxInfo } from './account';
import { Coins } from '@terra-money/terra.js';
import { formatAmountNumber, formatDenom } from '../utils/format';

export interface AccountTimeSeries {
    ts: string;
    v: {
        [key:string]: number;
    }
}

const accumulateReverseCoins = (balance: Coins, amountIn?: Coins, amountOut?: Coins): Coins => {
    let ret = balance;
    if (amountIn) {
        ret = ret.sub(amountIn);
    }
    if (amountOut) {
        ret = ret.add(amountOut);
    }
    return ret;
};

const explodeCoins = (c: Coins): { [key: string]: number } =>
    c
        .map((coin) => ({
                [formatDenom(coin.denom)]: formatAmountNumber(coin.amount.toString())
        }))
        .reduce(
            (prev, curr) => ({
                ...prev,
                ...curr
            }),
            {}
        );

const txsToAccountTimeSeries = (txs: TxInfo[], lastBalance: Coins): AccountTimeSeries[] => {
    let balance = new Coins(lastBalance);
    return txs
        .map((tx) => {
            // TODO: accumulate relevant fees
            balance = accumulateReverseCoins(balance, tx.amountIn, tx.amountOut);
            return {
                ts: tx.tx.timestamp,
                v: explodeCoins(balance)
            };
        })
        .filter((row) => Object.keys(row).length > 1);
}

export const accountTimeSeriesAtom = atom<AccountTimeSeries[]>((get) => {
    const balanceLoadable = get(accountBalanceAtom);
    const txsLoadable = get(accountTxsAtom);
    if (txsLoadable.data?.txs === undefined || balanceLoadable.data?.coins === undefined) {
        return []
    }
    const first = balanceLoadable.data.coins
        .map((c) => ({[formatDenom(c.denom)]: formatAmountNumber(c.amount.toString())}))
        .reduce((prev, curr) => ({ ...prev, ...curr }), {});
    const ret: AccountTimeSeries[] = [
        { ts: new Date().toISOString(), v: first },
        ...txsToAccountTimeSeries(txsLoadable.data.txs, balanceLoadable.data.coins),
    ];

    return ret;
});
