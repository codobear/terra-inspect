import { atom, useSetAtom, WritableAtom } from 'jotai';
import { Loadable } from '../types/Loadable';
import { PrimitiveAtom } from 'jotai/core/atom';
import { useEffect } from 'react';

type WriteGetter<T> = Parameters<WritableAtom<T, T>['write']>[0];
export type AsyncRead<T> = (get: WriteGetter<T>) => Promise<T>;

type FetchAtom<T> = WritableAtom<Loadable<T>, unknown>;

const fetchAtom = <T>(read: AsyncRead<T>, mapper?: ((current: T, prev?: T) => T)): FetchAtom<T> => {
    const fetchResultAtom: PrimitiveAtom<Loadable<T>> = atom({ loading: true } as Loadable<T>);
    return atom(
        (get) => get(fetchResultAtom),
        (_get, set, url) => {
            const run = async () => {
                try {
                    const data = await read(_get);
                    set(fetchResultAtom, (prev) => ({
                        ...prev,
                        loading: false,
                        data: mapper ? mapper(data, prev.data) : data
                    }));
                } catch (e) {
                    set(fetchResultAtom, { loading: false, error: `${e}` })
                }
            }
            run().catch(console.error);
        }
    );
};

type Mapper<T> = (current: T, prev?: T) => T;

export const loadableAtom = <T>(read: AsyncRead<T>, mapper?: Mapper<T>) => {
    const runFetchAtom = fetchAtom(read, mapper);
    runFetchAtom.onMount = (setAtom) => {
        setAtom(null);
    };
    return runFetchAtom;
};

export const loadableAtomWithHook = <T>(read: AsyncRead<T>, mapper?: Mapper<T>): [FetchAtom<T>, (deps: any[]) => void] => {
    const runFetchAtom = fetchAtom(read, mapper);
    const useFetchHook = (deps: Array<any>) => {
        const setAtom = useSetAtom(runFetchAtom);
        useEffect(() => {
            // wait for all deps to be defined
            if (!deps.some((d) => d === undefined)) {
                setAtom(deps);
            }
        }, [...deps, setAtom])
    };
    return [runFetchAtom, useFetchHook];
};

export const requestAtom = <T>(url: string) => loadableAtom<T>(async () => {
    const resp = await fetch(url);
    return await resp.json() as T;
});
