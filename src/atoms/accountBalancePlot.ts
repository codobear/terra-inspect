import { atom } from 'jotai';
import { accountBalanceAtom } from './account';
import { accountTimeSeriesAtom } from './accountTimeSeries';
import { formatDenom } from '../utils/format';


export const accountBalanceTypesAtom = atom((get) => {
    const balance = get(accountBalanceAtom);
    if (!balance.data) {
        return [];
    }
    return balance.data.coins.denoms().map(formatDenom);
});

export const accountBalancePlotCoinAtom = atom<string | null>(null);

export type PlotSeries = {
    date: string;
    v: number;
};

export type PlotData = {
    data: PlotSeries[];
    max?: number;
};

export const accountBalancePlotAtom = atom<PlotData>((get): PlotData => {
    const coin = get(accountBalancePlotCoinAtom);
    if (!coin) {
        return {  data: [] };
    }
    const ts = get(accountTimeSeriesAtom);
    const data = ts
        .filter(({ v }) => (v[coin] !== undefined))
        .map(({ ts, v }) => ({ date: ts, v: v[coin]}))
        .reverse();
    const max = data.reduce(
        (prev, curr) =>
            (prev === undefined || prev < curr.v) ? curr.v : prev,
        undefined as (number | undefined));
    return { data, max };
});
