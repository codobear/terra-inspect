import { atom, useAtomValue } from 'jotai';
import { TerraNetworkAssets } from '../types/assets';
import { terraAssetsAtom } from './assets';
import { LCDClient } from '@terra-money/terra.js';
import { createFcdClient } from '../api/fcdClient';

export const networkAtom = atom<'mainnet' | 'testnet' | 'local'>('mainnet');


const lcdUrls = {
    mainnet: 'https://lcd.terra.dev',
    testnet: 'https://bombay-lcd.terra.dev',
    local: 'http://localhost:1336'
};

export const lcdClientAtom = atom((get) => {
    const chainID = get(networkAtom);
    const url = lcdUrls[chainID];
    return new LCDClient({ chainID, URL: url });
});


export const networkAssetsAtom = atom<TerraNetworkAssets | null>((get) => {
    const network = get(networkAtom);
    const { data } = get(terraAssetsAtom);
    if (!data) {
        return null;
    }
    const { cw20Contracts, cw20Tokens, nftContracts } = data;
    return {
        cw20Contracts: cw20Contracts[network],
        cw20Tokens: cw20Tokens[network],
        nftContracts: nftContracts[network],
    };
});


const fcdUrls = {
    mainnet: 'https://fcd.terra.dev',
    testnet: 'https://bombay-fcd.terra.dev',
    local: 'http://localhost:3060'
};

export const fcdClientAtom = atom((get) => createFcdClient(fcdUrls[get(networkAtom)]));


