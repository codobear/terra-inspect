import { loadableAtom, requestAtom } from './loadableAtom';
import { ContractList, Dictionary, NFTContractList, TerraAssets, TokenList } from '../types/assets';
import apiClient from '../api/apiClient';

export const terraAssetsAtom = loadableAtom<TerraAssets>(async () => {
    const fetchAsset = async <T>(path: string): Promise<T> => {
        // const resp = await fetch(`https://assets.terra.money/${path}`);
        // return (await resp.json()) as T;
        const resp = await apiClient.get<T>(`https://assets.terra.money/${path}`);
        return resp.data;
    };
    const [
        cw20Contracts,
        cw20Tokens,
        nftContracts
    ] = await Promise.all([
        fetchAsset<Dictionary<ContractList>>('cw20/contracts.json'),
        fetchAsset<Dictionary<TokenList>>('cw20/tokens.json'),
        fetchAsset<Dictionary<NFTContractList>>('cw721/contracts.json')
    ]);
    return {
        cw20Contracts, cw20Tokens, nftContracts
    };
})
