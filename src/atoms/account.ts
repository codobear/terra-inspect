import { LogFinderAmountResult } from '@terra-money/log-finder-ruleset';
import { atom } from 'jotai';
import { fcdClientAtom, lcdClientAtom } from './network';
import { loadableAtom, loadableAtomWithHook } from './loadableAtom';
import { parseTxMsgsAmount } from '../utils/parse';
import { Coins } from '@terra-money/terra.js';

export interface TxInfo {
    tx: TxResponse;
    amount?: LogFinderAmountResult[][];
    amountIn?: Coins;
    amountOut?: Coins;
}

export interface AccountTxs {
    txs: TxInfo[];
    nextTxs?: number;
}

export interface AccountBalance {
    coins: Coins;
    next?: string;
    total?: number;
}


export const accountAddressAtom = atom<string | undefined>(undefined);

export const accountBalanceOffsetAtom = atom<string | undefined>(undefined);
export const [accountBalanceAtom, useAccountBalanceLoad] = loadableAtomWithHook<AccountBalance>(
    async (get) => {
        const lcdClient = get(lcdClientAtom);
        const address = get(accountAddressAtom);
        if (!address) {
            throw new Error('accountAddressAtom not set');
        }
        const params = {
            next_key: get(accountBalanceOffsetAtom)
        };
        const [coins, pagination] = await lcdClient.bank.balance(address, params);
        return {
            coins,
            next: pagination.next_key,
            total: pagination.total,
        } as AccountBalance;
    },
    (current, prev) => {
        const prevCoins = prev?.coins;
        if (prevCoins) {
            return {
                ...current,
                coins: prevCoins.add(current.coins)
            }
        }
        return current;
    }
);

export const accountTxsOffsetAtom = atom<number>(0);

const parseTx = (tx: TxResponse, account: string): Omit<TxInfo, 'tx'> => {
    const amount = parseTxMsgsAmount(tx, account);

    if (amount === undefined) {
        return {};
    }

    const coins: [Coins|null, Coins|null][] = amount.flat(2)
        .map((a) => {
            const { transformed: xformed } = a;
            if (xformed === undefined) {
                return [null, null];
            }
            const coins: Coins = Coins.fromString(xformed.amount);
            const spender = xformed.sender === account;
            const receiver = xformed.recipient === account;
            return [
                receiver ? coins : null,
                spender ? coins : null,
            ] as [Coins | null , Coins | null];
        });
    const [ amountIn, amountOut ] = coins.reduce(
        (prev, curr) => [
            curr[0] !== null ? prev[0]!.add(curr[0]) : prev[0],
            curr[1] !== null ? prev[1]!.add(curr[1]) : prev[1]
        ],
        [new Coins(), new Coins()]  as [Coins, Coins]
    );
    return { amount, amountIn: amountIn!, amountOut: amountOut! };
};


export const [accountTxsAtom, useAccountTxsLoad] = loadableAtomWithHook<AccountTxs>(
    async (get) : Promise<AccountTxs> => {
        const fcdClient = get(fcdClientAtom);
        const account = get(accountAddressAtom);
        const offset = get(accountTxsOffsetAtom);
        if (!account) {
            throw new Error('accountAddressAtom not set');
        }
        const data = await fcdClient.accountTxs({ account, offset });
        return {
            txs: data.txs.map((tx) => ({
                tx,
                // amount: parseTxMsgsAmount(tx, account)
                ...parseTx(tx, account)
            })),
            nextTxs: data.next
        };
    },
    // (current, prev) => {
    //     const prevTxs = prev?.txs;
    //     if (prevTxs) {
    //         return {
    //             txs: [...prevTxs, ...current.txs],
    //             nextTxs: current.nextTxs
    //         };
    //     }
    //     return current;
    // }
);



