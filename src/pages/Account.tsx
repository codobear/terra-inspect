import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import Loading from '../components/Loading';
import { LogFinderAmountResult } from '@terra-money/log-finder-ruleset';
import { Coin, Coins } from '@terra-money/terra.js';
import { LogFinderActionResult } from '@terra-money/log-finder-ruleset/dist/types';
import { Box, Button, DropButton, Heading, Text } from 'grommet';
import useTerraAssets from '../hooks/useTerraAssets';
import SaneBox from '../components/SaneBox';
import { formatAmount, formatDenom } from '../utils/format';
import { LinkNext } from 'grommet-icons';
import { ItemLimiter } from '../components/ItemLimiter';
import { Pill, PillProps } from '../components/Pill';
import { AnchorLink } from '../components/AnchorLink';
import { parseTxMsgs, parseTxMsgsAmount } from '../utils/parse';
import {
    accountAddressAtom,
    accountTxsOffsetAtom,
    accountBalanceAtom,
    accountTxsAtom, useAccountBalanceLoad, useAccountTxsLoad
} from '../atoms/account';
import { useAtom, useAtomValue, useSetAtom } from 'jotai';
import { useAtomDevtools } from 'jotai/devtools';
import { BalanceChart } from '../components/BalanceChart';


const ActionDisplay = ({ action }: { action: LogFinderActionResult }) => (
    <Box>
        <DropButton
            dropAlign={{ top: 'bottom', right: 'right' }}
            dropContent={<pre>
                {action.transformed?.canonicalMsg.join('\n')}
            </pre>}
            label={action.transformed?.msgType}
        />
    </Box>
)

const AmountDisplay = ({ amount, addr }: { amount: LogFinderAmountResult, addr: string }) => {
    const xformed = amount.transformed
    if (xformed) {
        const coins: Coins = Coins.fromString(xformed.amount);
        let denomNode = null;
        const spender = xformed.sender === addr;
        const receiver = xformed.recipient === addr;

        return (
            <SaneBox direction="column">
                {coins.map((coin) => {
                    const denom = coin.denom;
                    if (denom.substring(0, 5) === 'terra') {
                        denomNode = <AnnotateContractAddress contractAddress={denom} />;
                    } else {
                        denomNode = <>{formatDenom(denom)}</>
                    }
                    if (spender || receiver) {
                        const sign = spender ? '-' : '+';
                        return <SaneBox key={coin.denom}>{sign}{formatAmount(coin.amount.toString())} {denomNode}</SaneBox>
                    }
                    return null;
                }).filter((t) => t !== null)}
            </SaneBox>
        );
    }
    return null;
};

const AnnotateContractAddress = ({ contractAddress } : { contractAddress: string }) => {
    const terraAssets = useTerraAssets();
    const annot = (label: string, color: PillProps['color']) => (
        <Pill
            color={color}>
            <Text size="xsmall">{label}</Text>
        </Pill>
    );
    let ret = <></>;
    if (terraAssets) {
        const { cw20Contracts, cw20Tokens, nftContracts } = terraAssets;
        const cw20Contract = cw20Contracts[contractAddress];
        const cw20Token = cw20Tokens[contractAddress];
        const nftToken = nftContracts[contractAddress];
        if (cw20Contract) {
            ret = <>{cw20Contract.name}{annot('CW20 Contract', 'orange')}</>
        } else if (cw20Token) {
            ret = <>{cw20Token.symbol}{annot('CW20 Token', 'teal')}</>;
        } else if (nftToken) {
            ret = <>{nftToken.name}{annot('NFT Token', 'green')}</>
        }
    }

    return ret;
};


const EventKeyValue = ({ keyName, value }:{ keyName: string, value?: string }) => (
    <SaneBox
        direction="row"
        round="xsmall"
        gap="medium"
        margin="xsmall"
        pad={{vertical: 'xxsmall', horizontal: 'xsmall' }}
        background={{color: 'blue'}}
    >
        <Text size="xsmall" weight="bold">{keyName}</Text>
        <Text size="xsmall">{value}</Text>
        {keyName === 'contract_address' && value  && (
            <AnnotateContractAddress contractAddress={value} />
        )}
    </SaneBox>
);

type MsgDisplayProps = {
    action: LogFinderActionResult[];
    addr: string;
    amount?: LogFinderAmountResult[];
    txLog?: Log;
    msg: Msg;
};

const MsgDisplay = ({ amount, action, txLog, addr }: MsgDisplayProps) => {
    return (
        <Box gap="xsmall" border={{ style: 'dotted', size: 'xsmall' }} round="xsmall" pad="medium">
            {txLog && (
                <Box direction="column" gap="xsmall">
                    <Text size="xsmall">Events</Text>
                    {txLog.events.map((e, idx) => (
                        <Box direction="column" key={idx}>
                            {e.type}
                            <Box direction="row" wrap gap="xsmall" justify="start">
                                {e.attributes.map((attr, idx) => (
                                    <EventKeyValue key={idx} keyName={attr.key} value={attr.value} />
                                ))}
                            </Box>
                        </Box>
                    ))}
                </Box>
            )}
            <Text size="xsmall">Log Finder Actions:</Text>
            {action.map((m, idx) => <ActionDisplay action={m} key={idx} />)}
            <Text size="xsmall">Log Finder Amount:</Text>
            {amount && (
                amount.map(
                    (m, idx) => (
                        <AmountDisplay key={idx} addr={addr} amount={m}/>
                    )
                )
            )}
        </Box>
    )
}

const MsgLog = ({ txLog, msg }: { txLog?: Log, msg: Msg }) => (
    <Box direction="column" gap="xsmall">
        <Text size="medium" weight="bold">{msg.type}</Text>
        <Text size="xsmall">Events</Text>
        {txLog?.events.map((e, idx) => (
            <Box direction="column" key={idx}>
                {e.type}
                <Box direction="row" wrap gap="xsmall" justify="start">
                    {e.attributes.map((attr, idx) => (
                        <EventKeyValue key={idx} keyName={attr.key} value={attr.value} />
                    ))}
                </Box>
            </Box>
        ))}
    </Box>
);

const AddrLink = ({ destAddr, currAddr }: { destAddr: string; currAddr: string }) => {
    let ret = <Text>{destAddr}</Text>
    if (destAddr !== currAddr) {
        ret = <AnchorLink to={`/${destAddr}`}>{ret}</AnchorLink>
    }
    return ret;
}


type TxAmountDisplayProps = {
    addr: string;
    amount?: LogFinderAmountResult[];
};

const TxAmountDisplay = ({ addr, amount }: TxAmountDisplayProps) => {
    const makeAddrLink = (destAddr: string | undefined) => (
        destAddr ? (
            <AddrLink destAddr={destAddr} currAddr={addr}/>
        ) : (
            <Text color="muted">-</Text>
        )
    );

    return (
        <SaneBox direction="column" width="100%" align="stretch" border={{ style: 'solid', size: 'xsmall' }} pad="xsmall">
            {amount && amount.map((a, idx) => (
                <SaneBox key={idx} direction="row" justify="between" align="center"  border={{ style: 'solid', size: 'xsmall' }}>
                    <SaneBox direction="row" gap="medium" align="center">
                        {makeAddrLink(a.transformed?.sender)}
                        <LinkNext size="small"/>
                        {makeAddrLink(a.transformed?.recipient)}
                    </SaneBox>
                    <SaneBox>
                        <AmountDisplay amount={a} addr={addr} />
                    </SaneBox>
                </SaneBox>
            ))}
        </SaneBox>
    );
};

const MsgInfo = ({ msgs, logs, tags }: { msgs: Msg[], logs: Log[], tags: Tag[] }) => {
    const counts = msgs.reduce(
        (prev, curr) => ({
            ...prev,
            [curr.type]: (prev[curr.type] || 0) + 1
        }),
        {} as Record<string, number>
    );
    return (
        <SaneBox direction="row" gap="medium" wrap>
            {Object.entries(counts).map(([k, v]) => (
                <SaneBox key={k} direction="row" gap="medium" align="center">
                    <SaneBox pad="xxsmall" background="green" round="xsmall">{k}</SaneBox>
                    <SaneBox>{v}x</SaneBox>
                </SaneBox>
            ))}
        </SaneBox>
    );
};


const TxInfoDisplay = ({ txInfo, addr }:{ txInfo: TxResponse, addr: string }) => {
    const [showLogs, setShowLogs] = useState(false);
    const actions = parseTxMsgs(txInfo);
    const amounts = parseTxMsgsAmount(txInfo, addr);
    const msgs: Msg[] = txInfo.tx.value.msg;

    const body = txInfo.logs?.length ? (
        <SaneBox
            direction="column"
            gap="xxsmall"
        >
            {txInfo.tx.value.memo && (
                <SaneBox direction="column" gap="xsmall" pad={{ vertical: 'xxsmall' }}>
                    <Text size="xsmall">Memo</Text>
                    <Text>{txInfo.tx.value.memo}</Text>
                </SaneBox>
            )}
            <Box direction="row" gap="medium" pad={{ vertical: 'xxsmall' }}>
                <Text size="xsmall">Messages: {msgs.length || 0}</Text>
                <Text size="xsmall">Logs: {txInfo.logs?.length || 0}</Text>
                <Text size="xsmall">Tags: {txInfo.tags?.length || 0}</Text>
                <Text size="xsmall">Parsed actions: {actions.length}</Text>
                <Text size="xsmall">Parsed amounts: {amounts === undefined ? 0 : amounts.length}</Text>
            </Box>
            <MsgInfo msgs={msgs} logs={txInfo.logs} tags={txInfo.tags} />
            {showLogs && (
                <ItemLimiter
                    fill="horizontal"
                    limit={5}
                    items={msgs.map((msg, idx) => <MsgLog key={idx} msg={msg} txLog={txInfo.logs?.[idx]}/>)}
                />
            )}
            <Button size="small" label={showLogs ? 'Hide details' : 'Show details'} onClick={() => setShowLogs(!showLogs)}/>
            <ItemLimiter
                direction="column"
                gap="xxsmall"
                limit={10}
                items={amounts?.filter((a) => a !== undefined).map((amnt, idx) => (
                    <TxAmountDisplay
                        amount={amnt}
                        addr={addr}
                        key={idx}
                    />
                ))}
            />
        </SaneBox>
    ) : (
        <Text>{txInfo.raw_log}</Text>
    );
    return (
        <Box
            direction="column"
            border={{ size: "xsmall" }}
            pad="small"
            elevation="medium"
            round="xsmall"
        >
            <Text size="xsmall">TX: {txInfo.txhash}</Text>
            <Text size="small">{txInfo.timestamp}</Text>
            {body}
        </Box>
    )
};

const TxList = ({ address } : { address: string }) => {
    const [txs, setTxs] = useState<JSX.Element[]>([]);
    const { data } = useAtomValue(accountTxsAtom);
    const setOffset = useSetAtom(accountTxsOffsetAtom);
    useEffect(() => {
        if (data) {
            const newTxs = data.txs.map(({ tx }) => <TxInfoDisplay txInfo={tx} addr={address} key={tx.txhash} />);
            setTxs((txs) => [...txs, ...newTxs]);
        }
        return () => setTxs([]);
    }, [data, address]);
    if (!data) {
        return <Loading/>;
    }

    return (
        <Box
            width="100%"
            margin={{ vertical: 'xxsmall' }}
            direction="column"
            gap="xsmall"
            align="stretch"
        >
            {/*{data?.txs.map((tx) => <TxInfoDisplay txInfo={tx} addr={address} key={tx.txhash} />)}*/}
            {txs}
            {data?.nextTxs && (<Button label="More" onClick={() => data?.nextTxs !== undefined && setOffset(data?.nextTxs)} />)}
        </Box>
    );

};

const CoinBalance = ({ coin }: { coin: Coin  }) => (
    <SaneBox
        direction="column"
        justify="between"
        elevation="medium"
        round="small"
        border={{ size: 'xsmall' }}
        pad="medium"
    >
        <Text size="medium">{formatAmount(coin.amount.toString())}</Text>
        <Text size="xsmall">{formatDenom(coin.denom)}</Text>
    </SaneBox>
);

const AccountBalance = ({ address }: { address: string }) => {
    // const lcdClient = useLcdClient();
    // const [balance, setBalance] = useState<Coins | null>(null);
    // useEffect(() => {
    //     const run = async () => {
    //         const [coins, pg] = await lcdClient.bank.balance(address);
    //         setBalance(coins);
    //     };
    //     run().catch(console.error);
    // }, [lcdClient, address]);
    useAtomDevtools(accountBalanceAtom);
    const { data } = useAtomValue(accountBalanceAtom);
    if (!data) {
        return <Loading/>;
    }
    const balance = data.coins;
    return (
        <SaneBox wrap direction="row" align="center" justify="center">
            {balance.map((c) => (
                <SaneBox pad="medium">
                    <CoinBalance coin={c} key={c.denom} />
                </SaneBox>
            ))}
        </SaneBox>
    );
}


const Account = () => {
    const { addr = '' } = useParams();
    const [accountAddress, setAccountAddress] = useAtom(accountAddressAtom);
    useEffect(() => {
        if (addr !== accountAddress) {
            setAccountAddress(addr);
        }
    }, [addr, accountAddress, setAccountAddress]);
    const txOffset = useAtomValue(accountTxsOffsetAtom);
    useAccountBalanceLoad([accountAddress]);
    useAccountTxsLoad([accountAddress, txOffset]);

    return (
        <SaneBox direction="column" gap="small" align="stretch" margin="small">
            <SaneBox justify="center" align="center" direction="row">
                <Heading level="3">{addr}</Heading>
            </SaneBox>
            <AccountBalance address={addr} />
            <BalanceChart />
            <TxList address={addr} />
        </SaneBox>
    );
};

export default Account;
