import { Box, Heading } from 'grommet';
import { Coins, Dec, LCDClient, Msg, TxInfo, Validator } from '@terra-money/terra.js';
import { useEffect, useState } from 'react';


const addr = 'terra1x46rqay4d3cssq8gxxvqz8xt6nwlz4td20k38v';
const terra = new LCDClient({
    URL: 'http://localhost:1317',
    chainID: 'localterra'
});

const useTerra = () => (terra);


const MessageInfo: React.FC<{ msg: Msg }> = ({ msg }) => (
    <pre>
        {JSON.stringify(msg.toData(), null, 2)}
    </pre>
);

const Transaction: React.FC<{ tx: TxInfo }> = ({ tx }) => (
    <Box direction="column">
        <Box direction="row" justify="between" key={tx.txhash}>
            <div>
                {tx.timestamp}
            </div>
            <div>
                Memo: {tx.tx.body.memo}
            </div>
            <div>
                Messages: {tx.tx.body.messages.length}
            </div>
        </Box>
        <Box>
            {tx.tx.body.messages.map((msg, idx) => <MessageInfo msg={msg} key={idx} />)}
        </Box>

    </Box>
);


export const ExperimentAccount: React.FC<{}> = () => {
    const terra = useTerra();
    const [balance, setBalance] = useState<Coins>();
    const [boundValidators, setBoundValidators] = useState<Validator[]>()
    const [outboundTxs, setOutboundTxs] = useState<TxInfo[]>();
    const [inboundTxs, setInboundTxs] = useState<TxInfo[]>();
    useEffect(() => {
        const fetchBalanceData = async () => {
            const [coins] = await terra.bank.balance(addr);
            setBalance(coins);
        };
        const fetchStakingData = async () => {
            const [vals] = await terra.staking.bondedValidators(addr);
            setBoundValidators(vals);
        };
        const fetchTxData = async (k: string, setter: Function) => {
            const { txs } = await terra.tx.search({
                'pagination.limit': '100',
                events:[
                    // { key: 'message.sender', value: addr },
                    // { key: 'message.recipient', value: addr }
                    { key: k, value: addr }
                ]
            });
            // setOutboundTxs(txs);
            setter(txs);
        };
        fetchBalanceData().catch(console.error);
        fetchStakingData().catch(console.error);
        fetchTxData('message.sender', setOutboundTxs).catch(console.error)
        fetchTxData('transfer.recipient', setInboundTxs).catch(console.error)
    }, [terra]);

    return (
        <Box gap="medium" height={{ min: 'auto'}} width={{ min: 'auto' }}>
            <Heading level="1">{addr}</Heading>
            <Box gap="small">
                <Heading level="2">Balance</Heading>
                {balance?.map((c) => (
                    <Box flex direction="row" justify="between" key={c.denom}>
                        <div>
                            {c.denom}
                        </div>
                        <div>
                            {c.amount.toString()}
                        </div>
                    </Box>
                ))}
            </Box>
            <Box gap="small">
                <Heading level="2">Staking</Heading>
                {boundValidators?.map((val) => (
                    <Box flex direction="row" justify="between">
                        <div>{val.description.moniker}</div>
                        <div>{val.delegator_shares.toString()}</div>
                    </Box>
                ))}
            </Box>
            <Box gap="small">
                <Heading level="2">Txs</Heading>
                <Box>
                    <Heading level="3">Outbound</Heading>
                    {outboundTxs?.map((tx) => (<Transaction tx={tx} key={tx.txhash} />))}
                </Box>
                <Box>
                    <Heading level="3">Inbound</Heading>
                    {inboundTxs?.map((tx) => (<Transaction tx={tx} key={tx.txhash} />))}
                </Box>
            </Box>
        </Box>
    );
};
