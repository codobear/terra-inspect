import { Box, Form, FormField, Text, TextInput } from 'grommet';
import { useNavigate } from 'react-router-dom';

const FindAddress = () => {
    const navigate = useNavigate();
    return (
        <Box width={{ min: 'xlarge'}}>
            <Form<{ address: string }>
                onSubmit={({ value: { address } })  => navigate(`/${address}`)}
            >
                <FormField
                    name="address"
                    label="Address" htmlFor="tf-address"
                >
                    <TextInput name="address" size="xlarge" id="tf-address"/>
                </FormField>
            </Form>
        </Box>
    );
};

export default FindAddress;
