import { hpe } from 'grommet-theme-hpe';
import { deepMerge } from 'grommet/utils';


const theme = deepMerge({}, hpe, {
  global: {
    colors: {
      brand: "#228BE6",
    },
    font: {
      family: "Roboto",
      size: "18px",
      height: "20px",
    },
  },
});

export default theme;
