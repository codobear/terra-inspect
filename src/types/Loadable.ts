export interface Loadable<T> {
    data?: T;
    loading: boolean;
    error?: string;
}

export const makeLoadable = <T = any>() => ({ loading: false });