interface Whitelist {
    protocol: string;
    symbol: string;
    token: string;
    icon?: string;
}

interface Contracts {
    protocol: string;
    name: string;
    icon: string;
}

interface NFTContracts {
    name: string;
    icon: string;
    contract: string;
}

interface ChainOption {
    name: string;
    chainID: string;
    lcd: string;
    mantle: string;
}

export type Dictionary<T> = Record<string, T>;

export type ContractList = Dictionary<Contracts>;
export type NFTContractList = Dictionary<NFTContracts>;
export type TokenList = Dictionary<Whitelist>;

export type TerraNetworkAssets = {
    cw20Contracts: ContractList;
    cw20Tokens: TokenList;
    nftContracts: NFTContractList;
};

export type TerraAssets = {
    cw20Contracts: Dictionary<ContractList>;
    cw20Tokens: Dictionary<TokenList>;
    nftContracts: Dictionary<NFTContractList>;
}
