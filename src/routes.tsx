import { Routes, Route } from 'react-router-dom';
import Account from './pages/Account';
import { ExperimentAccount } from './pages/Experiment';
import FindAddress from './pages/FindAddress';

export default (
    <Routes>
        <Route path="/:addr" element={<Account />} />
        <Route index element={<FindAddress />} />
    </Routes>
);
