import apiClient from './apiClient';
import { AxiosRequestConfig } from 'axios';

type GetFunc = <T>(path: string, config?: AxiosRequestConfig) => Promise<T>;

export interface FcdClient {
    baseUrl: string;
    get: GetFunc;
    accountTxs: (params: {account: string, offset?: number, limit?: number}) => Promise<TxSearchResponse>
}

export const createFcdClient = (baseUrl: string): FcdClient => {
    const get: GetFunc = async <T>(path: string, config?: AxiosRequestConfig) => {
        const resp = await apiClient.get<T>(baseUrl + path, config);
        return resp.data;
    };
    return ({
        baseUrl,
        get,
        accountTxs: async ({
            account,
            offset = 0,
            limit = 100
        }) => await get<TxSearchResponse>('/v1/txs', { params: { account, offset, limit } }),
    });
};
