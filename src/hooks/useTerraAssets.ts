import { useAtomValue } from 'jotai';
import { TerraNetworkAssets } from '../types/assets';
import { networkAssetsAtom } from '../atoms/network';


const useTerraAssets = (): TerraNetworkAssets | null => useAtomValue(networkAssetsAtom);

export default useTerraAssets;
