import { useEffect, useState } from 'react';
import apiClient from '../api/apiClient';

const useFcdUrl = () =>
    // 'http://localhost:3060';
    'https://fcd.terra.dev';

const useFcdRequest = <T>(
    url: string,
    offset?: number,
    limit?: number,
    account?: string
) : { data: T | undefined, isLoading: boolean } => {
    const baseUrl = useFcdUrl();
    const [data, setData] = useState<T>();
    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        const task = async (): Promise<void> => {
            const params = {
                offset, limit, account
            };
            try {
                const res = await apiClient.get<T>(
                    `${baseUrl}${url}`,
                    { params }
                );
                setData(res.data);
            } catch (e) {
                console.error(e);
            }
            setIsLoading(false);
        };

        task().catch(console.error);
    }, [url, baseUrl, offset, limit, account]);
    return { data, isLoading }
};

export default useFcdRequest;
