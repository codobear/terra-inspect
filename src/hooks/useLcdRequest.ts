import { LCDClient } from '@terra-money/terra.js';
import { useMemo } from 'react';
import { useAtomValue } from 'jotai';
import { lcdClientAtom, networkAtom } from '../atoms/network';

// const lcdUrls = { mainnet: 'https://lcd.terra.dev', testnet: 'https://bombay-lcd.terra.dev', local: 'http://localhost:1336'}
//
// const useLcdClient = () => {
//     const chainID = useAtomValue(networkAtom);
//     const url = lcdUrls[chainID];
//     return useMemo(
//         () => new LCDClient({ chainID, URL: url }),
//         [chainID, url]
//     );
// };

const useLcdClient = () => useAtomValue(lcdClientAtom);

export default useLcdClient;
