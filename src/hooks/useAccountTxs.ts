import { LogFinderAmountResult } from '@terra-money/log-finder-ruleset';

export interface TxInfo {
    tx: TxResponse;
    amount?: LogFinderAmountResult[][];
}

export interface AccountTxs {
    txs: TxInfo[];
    nextTxs?: string;
}

export const useAccountTxs = (addr: string) => {

};