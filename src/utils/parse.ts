import {
    createActionRuleSet,
    createAmountRuleSet,
    createLogMatcherForActions,
    createLogMatcherForAmounts,
    getTxAmounts,
    getTxCanonicalMsgs,
    LogFinderAmountResult
} from '@terra-money/log-finder-ruleset';
import { LogFinderActionResult } from '@terra-money/log-finder-ruleset/dist/types';

const actionRuleSet = createActionRuleSet('mainnet');
const actionLogMatcher = createLogMatcherForActions(actionRuleSet);
const amountRuleSet = createAmountRuleSet();
const amountLogMatcher = createLogMatcherForAmounts(amountRuleSet);
export const parseTxMsgs = (txInfo: TxResponse): LogFinderActionResult[][] => getTxCanonicalMsgs(JSON.stringify(txInfo), actionLogMatcher);
export const parseTxMsgsAmount = (txInfo: TxResponse, addr: string): LogFinderAmountResult[][] | undefined => getTxAmounts(JSON.stringify(txInfo), amountLogMatcher, addr);