import { Coin, Denom } from '@terra-money/terra.js';
import BigNumber from 'bignumber.js';

export const isIbcDenom = (denom: Denom): boolean => denom.substring(0, 4) === 'ibc/';

// export const formatAmount = (c: Coin) => {
//     return new BigNumber(c.amount.toString())
//         .decimalPlaces(6, BigNumber.ROUND_DOWN)
//         .toFixed(6);
// };


export const formatAmount = (amount: BigNumber.Value, decimals = 6): string =>
    new BigNumber(amount)
        .div(new BigNumber(10).pow(decimals))
        .decimalPlaces(6, BigNumber.ROUND_DOWN)
        .toFormat(6);

export const formatAmountNumber = (amount: BigNumber.Value, decimals = 6): number =>
    new BigNumber(amount)
        .div(new BigNumber(10).pow(decimals))
        .decimalPlaces(6, BigNumber.ROUND_DOWN)
        .toNumber();


export const formatDenom = (denom: string) => {
    let f = denom;
    if (f[0] === 'u') {
        f = f.slice(1);
        if (f.length > 3) {
            if (f === 'luna') {
                f = 'Luna';
            }
        } else {
            f = f.toUpperCase() + 'T';
        }
    }
    return f;
};

export const formatCoin = (c: Coin): string => {
    const { denom } = c;
    if (isIbcDenom(denom)) {
        return c.toString();
    }
    const amount = formatAmount(c.amount.toString());
    const f = formatDenom(denom);

    return `${f} ${amount}`;
};
