import { useState } from "react";
import { Box, Button, Heading, Grommet, ResponsiveContext, RadioButtonGroup, Text } from "grommet";
import { Menu } from "grommet-icons";
import AppHeader from "./components/AppHeader";
import AppSidebar from "./components/AppSidebar";
import theme from "./theme";
import { HashRouter } from 'react-router-dom';
import routes from './routes'
import { useAtom, useAtomValue } from 'jotai';
import { terraAssetsAtom } from './atoms/assets';
import { themeAtom } from './atoms/theme';
import SaneBox from './components/SaneBox';
import { AnchorLink } from './components/AnchorLink';

function App() {
    const [showSidebar, setShowSidebar] = useState(false);
    const terraAssets = useAtomValue(terraAssetsAtom);
    const [themeMode, setThemeMode] = useAtom(themeAtom);

    const body = terraAssets.loading ? <>Loading</> : terraAssets.error ? <>{terraAssets.error}</> : routes;

    return (
        <HashRouter>
            <Grommet theme={theme} themeMode={themeMode} full>
                <ResponsiveContext.Consumer>
                    {(size) => (
                        <Box flex height="100%" width="100%">
                            <AppHeader>
                                <AnchorLink to="/">
                                    <Heading level="3" margin="none">
                                        Terra Inspect
                                    </Heading>
                                </AnchorLink>
                                <Button
                                    icon={<Menu />}
                                    onClick={() => setShowSidebar(!showSidebar)}
                                />
                            </AppHeader>
                            <Box direction="row" flex fill="horizontal">
                                <Box overflow="scroll" fill="horizontal">
                                    <Box flex align="center" justify="center" height={{ min: 'auto' }} width={{ min: 'auto' }}>
                                        {/*app body*/}
                                        {/*<Account/>*/}
                                        {/*{routes}*/}
                                        {body}
                                    </Box>
                                </Box>
                                <AppSidebar
                                    size={size}
                                    showSidebar={showSidebar}
                                    onClose={() => setShowSidebar(false)}
                                >
                                    <SaneBox>
                                        <Text>Theme</Text>
                                        <RadioButtonGroup
                                            name="thme"
                                            options={[
                                                { label: 'Light', value: 'light' },
                                                { label: 'Dark', value: 'dark' }
                                            ]}
                                            value={themeMode}
                                            onChange={({ target: {value}}) => setThemeMode(value as any)}
                                        />
                                    </SaneBox>
                                </AppSidebar>
                            </Box>
                        </Box>
                    )}
                </ResponsiveContext.Consumer>
            </Grommet>
        </HashRouter>
    );
}

export default App;
