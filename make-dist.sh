#!/bin/bash
cd $(dirname $0)
rm -f dist.zip
npm run build
cd build
zip -r ../dist.zip *
